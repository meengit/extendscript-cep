![ES Projekt Logo](logo/export/master.png)

Ressourcen für [Adobe ExtendScript](https://en.wikipedia.org/wiki/ExtendScript) und [CEP](https://github.com/Adobe-CEP), hauptsächlich mit Bezug zu [Adobe InDesign](https://www.adobe.com/products/indesign.html).

## Deprecated!

Wir sind umgezogen und neu als GitLab Group organisiert: <https://gitlab.com/extendscript-cep>

